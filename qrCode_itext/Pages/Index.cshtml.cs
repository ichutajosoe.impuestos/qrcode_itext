﻿using System;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using qrCode_itext.Reports;
using QRCoder;

namespace qrCode_itext.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IWebHostEnvironment _oHostEnvironment;

        public IndexModel(ILogger<IndexModel> logger, IWebHostEnvironment oHostEnvironment)
        {
            _logger = logger;
            _oHostEnvironment = oHostEnvironment;
        }

        public void OnGet()
        {
        }
        
        public FileContentResult OnGetReporte(int id)
        {
            //ReportCertificacionF200 rpt = new ReportCertificacionF200(_oHostEnvironment);
            //var qrData = QrCodeGenerator("JOSOE ERNESTO ICHUTA TUCO");
            //var dto = BitmapToBytesCode(qrData);
            //var pdf = File(rpt.Report(dto), "application/pdf");
            //return pdf;

            // ReportCertificacionF400 rpt = new ReportCertificacionF400(_oHostEnvironment);
            // var qrData = QrCodeGenerator("JOSOE ERNESTO ICHUTA TUCO");
            // var dto = BitmapToBytesCode(qrData);
            // var pdf = File(rpt.Report(dto), "application/pdf");
            // return pdf;

            // CertificadoBoleta1000 rpt = new CertificadoBoleta1000(_oHostEnvironment);
            // var dto = new byte[]{0,0,0,0};
            // var pdf = File(rpt.Report(dto), "application/pdf");
            // return pdf;

            // ReportDeudaCalculadoraTrib rpt = new ReportDeudaCalculadoraTrib(_oHostEnvironment);
            // var dto = new byte[] {0, 0, 0, 0,};
            // var pdf = File(rpt.Report(dto), "application/pdf");
            // return pdf;

            ReportExtractoTributario rpt = new ReportExtractoTributario(_oHostEnvironment);
            var dto = new byte[] { 0, 0, 0, 0, };
            var pdf = File(rpt.Report(dto), "application/pdf");
            return pdf;

            //ReportCertificacionF500 rpt = new ReportCertificacionF500(_oHostEnvironment);
            //var dto = new byte[]{0,0,0,0};
            //var pdf = File(rpt.Report(dto), "application/pdf");
            //return pdf;
        }

        public Bitmap QrCodeGenerator(string txtQRCode) {      
            QRCodeGenerator _qrCode = new QRCodeGenerator();      
            QRCodeData _qrCodeData = _qrCode.CreateQrCode(txtQRCode,QRCodeGenerator.ECCLevel.Q);      
            QRCode qrCode = new QRCode(_qrCodeData);      
            // Bitmap qrCodeImage = qrCode.GetGraphic(20);
            var pathImage = _oHostEnvironment.WebRootPath;
            Bitmap logoSin = (Bitmap)Image.FromFile(pathImage+"/images/logo.png");
            Bitmap qrCodeImage = qrCode.GetGraphic(20, Color.Black, Color.White, logoSin,30,9,false);

            return qrCodeImage;      
        }
        private static Byte[] BitmapToBytesCode(Bitmap image)      
        {      
            using (MemoryStream stream = new MemoryStream())      
            {      
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);      
                return stream.ToArray();      
            }      
        }  
    }
}