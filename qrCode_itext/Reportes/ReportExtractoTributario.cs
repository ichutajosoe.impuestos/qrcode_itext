using System;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Hosting;
using qrCode_itext.Reportes;

namespace qrCode_itext.Reports
{
    public class ReportExtractoTributario
    {
        private IWebHostEnvironment _oHostEnvironment;
        public ReportExtractoTributario(IWebHostEnvironment oHostEnvironment)
        {
            _oHostEnvironment = oHostEnvironment;
        }
        
        #region Declaracion Cabecera
        private string titulo = "Extracto Tributario";
        
        #endregion
        
        
        public byte[] Report(byte[] dto)
        {
            /*_datosReporte = datosReporte;*/
            
            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.LETTER);
            //document.SetMargins(30f, 55f, 40f, 55f);
            document.SetMargins(70f, 55f, 70f, 55f);

            // document.Add(new Paragraph("Hello world!"));


            string path = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine = System.IO.Path.Combine(path, "sin.png");
            //var logo = new Image(ImageDataFactory.Create(imgCombine));

            pdf.AddEventHandler(PdfDocumentEvent.START_PAGE, new HeaderEventHandler(imgCombine));
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, new FooterEventHandler());



            ////header
            //#region HEADER
            ////LOGO
            //string path = _oHostEnvironment.WebRootPath + "/images";
            //string imgCombine = System.IO.Path.Combine(path, "sin.png");
            //var logo = new Image(ImageDataFactory.Create(imgCombine));
            //logo.Scale(3, 3);

            ////Tabla HEADER
            //float[] cellWidth0 = { 80f, 20f };
            //Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            //// table0.SetFontSize(11);

            //Cell tituloCell = new Cell().Add(new Paragraph(titulo).SetTextAlignment(TextAlignment.LEFT).SetFontSize(14).SetPaddingTop(10));
            //table0.AddCell(tituloCell.SetMargin(0).SetPaddingLeft(3).SetBold().SetBorder(Border.NO_BORDER));

            //Cell imgCell = new Cell(2,1).Add(logo.SetAutoScale(true));  //cell para el LOGO
            //table0.AddCell(imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0));

            //table0.AddCell(DatosCelda("R-0436", false, false, TextAlignment.LEFT).SetFontSize(8));

            //document.Add(table0);
            //#endregion

            //Line Separacion
            document.Add(new LineSeparator(new SolidLine(1)).SetPadding(3));
            document.Add(new Paragraph(" ").SetMargins(5,0,0,0));
            
            //cabecera Datos
            #region CABECERA DATOS
            float[] cellWidth1 = { 17f, 83f };
            Table table1 = new Table(UnitValue.CreatePercentArray(cellWidth1)).UseAllAvailableWidth();
            table1.SetFontSize(8);
            
            table1.AddCell(DatosCelda("Jurisdicción:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("4091683012", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("NIT-Razón Social:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("ARANIBAR VARGAS JOSE ALBERTO", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Actividad Economica:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("19990201427", false, false, TextAlignment.LEFT));
            
            table1.AddCell(DatosCelda("Dirección:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("291629000011", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Período Consulta:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda(DateTime.Now.ToString("dd/MM/yyyy"), false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Usuario:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("0%", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Fecha Impresión:", true, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("0%", false, false, TextAlignment.LEFT));
            
            document.Add(table1);
            #endregion

            for (int i = 0; i < 15; i++)
            {
                //Formulario
                document.Add(LetrasBody("Formulario: " + "200" + " RC-IVA", 8, false, false, 2, 2, TextAlignment.LEFT));
                //cabecera tabla

                #region CABECERA TABLA

                float[] cellWidth2 = {4f, 4f, 5f, 10f, 13f, 12f, 10f, 16f, 10f, 16f};
                Table table2 = new Table(UnitValue.CreatePercentArray(cellWidth2)).UseAllAvailableWidth();
                table2.SetFontSize(6);

                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Nro. Sec."))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Mes"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Año"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Fecha Pago"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Número de Orden"))));

                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Saldo Favor Fisco"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Importe Pagado"))));
                table2.AddCell(
                    PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Monto Declarado en Casilla Valores"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("Otros Pagos"))));
                table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("DDJJ Rectificatoria"))));

                document.Add(table2);

                #endregion

                //Contenido tabla

                #region CONTENIDO TABLA DINAMICA

                //Separacion
                document.Add(new Paragraph(" ").SetMargins(2, 0, 0, 0));
                float[] cellWidth3 = {4f, 4f, 5f, 10f, 13f, 12f, 10f, 16f, 10f, 16f};
                Table table3 = new Table(UnitValue.CreatePercentArray(cellWidth3)).UseAllAvailableWidth();
                table3.SetFontSize(6);

                for (int j = 0; j < 5; j++)
                {
                    table3.AddCell(DatosCelda(j.ToString(), false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("12", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("2020", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda(DateTime.Now.ToString("dd/MM/yyyy"), false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));

                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                    table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                }

                document.Add(table3);

                #endregion
            }
            

            document.Close();

            return stream.ToArray();
        }

        private Paragraph LetrasBody(string letras, int sizeLetra, bool isBold, bool isBlue, int separacionTop, int separacionBottom, TextAlignment posicionLetra)
        {
            Paragraph letrasBody = new Paragraph(letras);
            letrasBody.SetTextAlignment(posicionLetra);
            letrasBody.SetFontSize(sizeLetra);
            letrasBody.SetPaddingTop(separacionTop); //separacion con bloque de arriba
            letrasBody.SetPaddingBottom(separacionBottom);
            letrasBody.SetMarginBottom(separacionBottom);
            letrasBody.SetMarginTop(1);
            if (isBlue)
                letrasBody.SetFontColor(new DeviceRgb(0, 64, 128));
            if (isBold)
                letrasBody.SetBold();
            return letrasBody;
        }
        private Paragraph LetrasCabeceraTabla(string letras)
        {
            Paragraph text = new Paragraph(letras);
            text.SetTextAlignment(TextAlignment.CENTER);
            text.SetBold();
            return text;
        }

        private Cell PropiedadesCelda(Cell celda)
        {
            celda.SetMargin(0);
            celda.SetBackgroundColor(ColorConstants.LIGHT_GRAY);
            celda.SetPadding(0);
            celda.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            return celda;
        }

        private Cell DatosCelda(string contenidoCelda, bool isBold, bool conBorde, TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);
            
            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(229, 231, 233)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);

            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(1);
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            if (posicionLetra == TextAlignment.LEFT)
                datosCelda.SetPaddingLeft(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if (isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if (!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
            // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            return datosCelda;
        }
    }
}