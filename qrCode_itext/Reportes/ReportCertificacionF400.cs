using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;

namespace qrCode_itext.Reports
{
    public class ReportCertificacionF400
    {
        private IWebHostEnvironment _oHostEnvironment;
        public ReportCertificacionF400(IWebHostEnvironment oHostEnvironment)
        {
            _oHostEnvironment = oHostEnvironment;
        }

        #region Declaracion Cabecera

        private string titulo = "FORMULARIO: 400 \nVERSIÓN: 3 \nIMPUESTO A LAS TRANSACCIONES";
        private string cabecera = "(A) CABECERA DE LA DECLARACION JURADA";
        private string razonSocialTitle = " NOMBRE(S) Y APELLIDO(S) O RAZON SOCIAL DEL SUJETO PASIVO";
        private string incisoC = "B) DETERMINACION DEL SALDO DEFINITIVO A FAVOR DEL FISCO O DEL CONTRIBUYENTE";
        #endregion

        #region Declaracion Rubro 1
        private string rubro1 = "RUBRO 1) DETERMINACION DE LA BASE IMPONIBLE E IMPUESTOL";
        private string rubro1_a = "Total ingresos brutos devengados y/o percibidos en especie (incluye ingresos exentos)";
        private string rubro1_b = "Ingresos exentos";
        private string rubro1_c = "Base imponible (C13 - C32)";
        private string rubro1_d = "Impuesto determinado (C24 * 3%)";
        #endregion

        #region Declaracion Rubro 2
        private string rubro2 = "RUBRO 2) DETERMINACION DEL SALDO";
        private string rubro2_a = "IUE pagado a compensar (1ra. Instancia o Saldo IUE a compensar del Form. 400, C619 del periodo anterior)";
        private string rubro2_b = "Saldo de Impuesto Determinado a favor del Fisco (C909-C664; si > 0)";
        private string rubro2_c = "Pagos a Cuenta realizados en DD.JJ. y/o Boletas de Pago correspondientes al periodo que se declara";
        private string rubro2_d = "Saldo de Pagos a Cuenta del periodo anterior a compensar (C747 del Formulario del periodo anterior)";
        private string rubro2_e = "Saldo por Pagos a Cuenta a favor del Contribuyente (C622 + C640 - C1001; si > 0)";
        private string rubro2_f = "Saldo a favor del Fisco (C1001-C622-C640; si > 0)";
        #endregion

        #region Declaracion Rubro 3
        private string rubro3 = "RUBRO 3) DETERMINACION DE LA DEUDA TRIBUTARIA";
        private string rubro3_a = "Tributo omitido (C996)";
        private string rubro3_b = "Actualización de valor sobre el Tributo Omitido";
        private string rubro3_c = "Intereses sobre Tributo Omitido Actualizado";
        private string rubro3_d = "Multa por incumplimiento al Deber Formal (IDF) por presentación fuera de plazo";
        private string rubro3_e = "Multa por IDF por Incremento del Impuesto Determinado en DD.JJ. Rectificatoria presentada fuera de plazo";
        private string rubro3_f = "Total Deuda Tributaria (C924+C925+C938+C954+C967)";
        #endregion

        #region Declaracion Rubro 4
        private string rubro4 = "RUBRO 4) SALDO DEFINITIVO";
        private string rubro4_a = "Saldo Definitivo de IUE a compensar para el siguiente periodo (C664 - C909; si > 0)";
        private string rubro4_b = "Saldo definitivo por Pagos a Cuenta a favor del Contribuyente para el siguiente periodo (C643-C955; si > 0)";
        private string rubro4_c = "Saldo definitivo a favor del Fisco (C996 ó (C955 - C643) según corresponda ; si > 0)";
        #endregion

        #region Declaracion Rubro 5
        private string rubro5 = "RUBRO 5)  IMPORTE DE PAGO";
        private string rubro5_a = "Pago en valores (sujeto a verificación y confirmación por el SIN)";
        private string rubro5_b = "Pago en efectivo (C646 - C677); si > 0)";
        #endregion

        private string firmaDeclaracion = "JURO LA EXACTITUD DE LA PRESENTE DECLARACION (Artículo N° 22 y Artículo N°78 Parrafo I de la Ley N°2492 Código Tributario Boliviano)" +
                                          "\n \n \n \t -------------------------------------------------------------" +
                                          "\n \t \t Firma del sujetopasivo o tercero responsable";

        private string footer = "Impresion con validez probatoria conforme al art. 79 de la Ley N° 2492 y 3er. Párrafo del art. 7 del D.S. N° 27310";


        public byte[] Report(byte[] dto)
        {
            /*_datosReporte = datosReporte;*/


            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.LETTER);
            document.SetMargins(30f, 55f, 40f, 55f);

            // document.Add(new Paragraph("Hello world!"));

            //LOGO
            string path = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine = System.IO.Path.Combine(path, "sin.png");
            var logo = new Image(ImageDataFactory.Create(imgCombine));
            logo.Scale(3, 3);
            // document.Add(logo);
            var qrData = dto;
            Image qrImage = new Image(ImageDataFactory.Create(qrData));

            //Tabla HEADER
            float[] cellWidth0 = { 25f, 63f, 12f };
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            Cell imgCell = new Cell().Add(logo.SetAutoScale(true));  //cell para el LOGO
            imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0);
            Cell imgCellQr = new Cell().Add(qrImage.SetWidth(70)); //cell para el QR
            imgCellQr.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0);
            table0.SetFontSize(11);

            table0.AddCell(imgCell);
            table0.AddCell(DatosCelda(titulo, false, false, TextAlignment.LEFT).SetPadding(5));
            table0.AddCell(imgCellQr);

            document.Add(table0);

            //cabecera

            #region TABLA CABECERA
            // Table cabecera
            document.Add(LetrasBody(cabecera, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth1 = { 30f, 10f, 10f, 30f, 20f };
            Table table1 = new Table(UnitValue.CreatePercentArray(cellWidth1)).UseAllAvailableWidth();
            table1.SetFontSize(7);

            Cell razonSocial = new Cell(1, 5).Add(new Paragraph(razonSocialTitle)); //cell para el QR
            table1.AddCell(razonSocial.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            Cell razonSocialData = new Cell(1, 5).Add(new Paragraph("cabecera")); //cell para el QR
            table1.AddCell(razonSocialData.SetMargin(0).SetPadding(0).SetTextAlignment(TextAlignment.CENTER));

            table1.AddCell(DatosCelda(" NIT", false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            Cell periodoCell = new Cell(1, 2).Add(new Paragraph(" PERIODO").SetTextAlignment(TextAlignment.LEFT)); //cell para el QR
            table1.AddCell(periodoCell.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            table1.AddCell(DatosCelda(" NÚMERO DE ORDEN", false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table1.AddCell(DatosCelda(" DD.JJ. ORIGINAL", false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));

            table1.AddCell(DatosCelda("8302579845", false, true, TextAlignment.CENTER));
            table1.AddCell(DatosCelda("Mes: " + "12", false, true, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Año: " + "2020", false, true, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("456789132", false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table1.AddCell(DatosCelda("Cod. 534: x", false, true, TextAlignment.LEFT));

            document.Add(table1);
            #endregion

            //Insiso C
            document.Add(LetrasBody(incisoC, 8, false, true, 0, 0, TextAlignment.LEFT));
            // Table Rubro1
            #region RUBRO1
            document.Add(LetrasBody(rubro1, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth2 = { 4f, 70f, 6f, 20f };
            Table table2 = new Table(UnitValue.CreatePercentArray(cellWidth2)).UseAllAvailableWidth();
            table2.SetFontSize(7);

            table2.AddCell(DatosCelda("Fila", false, true, TextAlignment.CENTER).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("DETERMINACIÓN DE LA BASE IMPONIBLE E IMPUESTO", false, true, TextAlignment.LEFT).SetPaddingLeft(3).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("Cód.", false, true, TextAlignment.CENTER).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("IMPORTE (EN BOLIVIANOS)", false, true, TextAlignment.RIGHT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));

            table2.AddCell(DatosCelda("a", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_a, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("13", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table2.AddCell(DatosCelda("b", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_b, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("32", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table2.AddCell(DatosCelda("c", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_c, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("24", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table2.AddCell(DatosCelda("d", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_d, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("909", false, true, TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            document.Add(table2);
            #endregion
            // Table Rubro2
            #region RUBRO2
            document.Add(LetrasBody(rubro2, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth3 = { 4f, 70f, 6f, 20f };
            Table table3 = new Table(UnitValue.CreatePercentArray(cellWidth3)).UseAllAvailableWidth();
            table3.SetFontSize(7);

            table3.AddCell(DatosCelda("a", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_a, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("664", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table3.AddCell(DatosCelda("b", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_b, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("1001", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table3.AddCell(DatosCelda("c", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_c, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("622", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table3.AddCell(DatosCelda("d", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_d, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("640", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table3.AddCell(DatosCelda("e", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_e, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("643", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table3.AddCell(DatosCelda("f", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_f, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("996", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            document.Add(table3);
            #endregion
            // Table Rubro3
            #region RUBRO3
            document.Add(LetrasBody(rubro3, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth4 = { 4f, 70f, 6f, 20f };
            Table table4 = new Table(UnitValue.CreatePercentArray(cellWidth4)).UseAllAvailableWidth();
            table4.SetFontSize(7);

            table4.AddCell(DatosCelda("a", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_a, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("924", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table4.AddCell(DatosCelda("b", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_b, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("925", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table4.AddCell(DatosCelda("c", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_c, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("938", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table4.AddCell(DatosCelda("d", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_d, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("954", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table4.AddCell(DatosCelda("e", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_e, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("967", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table4.AddCell(DatosCelda("f", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_f, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("955", false, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            document.Add(table4);
            #endregion
            // Table Rubro4
            #region RUBRO4
            document.Add(LetrasBody(rubro4, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth5 = { 4f, 70f, 6f, 20f };
            Table table5 = new Table(UnitValue.CreatePercentArray(cellWidth5)).UseAllAvailableWidth();
            table5.SetFontSize(7);

            table5.AddCell(DatosCelda("a", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_a, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("619", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table5.AddCell(DatosCelda("b", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_b, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("747", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table5.AddCell(DatosCelda("c", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_c, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("646", false, true, TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            document.Add(table5);
            #endregion
            // Table Rubro5
            #region RUBRO5
            document.Add(LetrasBody(rubro5, 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth6 = { 4f, 70f, 6f, 20f };
            Table table6 = new Table(UnitValue.CreatePercentArray(cellWidth6)).UseAllAvailableWidth();
            table6.SetFontSize(7);

            table6.AddCell(DatosCelda("a", false, true, TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_a, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("677", false, true, TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            table6.AddCell(DatosCelda("b", false, true, TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_b, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("576", false, true, TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            document.Add(table6);
            #endregion

            // Table Firma
            #region FIRMA Y CI

            document.Add(new Paragraph(" "));

            float[] cellWidth9 = { 50f, 50f };
            Table table9 = new Table(UnitValue.CreatePercentArray(cellWidth9)).UseAllAvailableWidth();
            Cell firmaCell = new Cell(2, 1).Add(new Paragraph(firmaDeclaracion).SetTextAlignment(TextAlignment.CENTER));
            firmaCell.SetPadding(0).SetMargin(0);
            table9.SetFontSize(7);

            table9.AddCell(firmaCell);
            table9.AddCell(DatosCelda("Aclaración de Firma: \n \n \n \n", false, true, TextAlignment.LEFT));
            table9.AddCell(DatosCelda("C.I.:", false, true, TextAlignment.LEFT).SetVerticalAlignment(VerticalAlignment.TOP));

            document.Add(table9);
            #endregion

            //footer
            document.Add(LetrasBody(footer, 8, false, false, 2, 2, TextAlignment.LEFT));
            //Numeero de certificacion, Cod seguridad y Fecha
            #region Num Certificacion, Seguridad y Fecha
            float[] cellWidth10 = { 34f, 33f, 33f };
            Table table10 = new Table(UnitValue.CreatePercentArray(cellWidth10)).UseAllAvailableWidth();
            table10.SetFontSize(7);

            table10.AddCell(DatosCelda("Código de Certificación: " + "123456", false, false, TextAlignment.LEFT));
            table10.AddCell(DatosCelda("Código de Seguridad: " + "798465", false, false, TextAlignment.CENTER));
            table10.AddCell(DatosCelda("Fecha de Certificación: " + DateTime.Now.ToString("dd/MM/yyyy"), false, false, TextAlignment.RIGHT));

            document.Add(table10);
            #endregion

            document.Close();

            return stream.ToArray();
        }

        private Paragraph LetrasBody(string letras, int sizeLetra, bool isBold, bool isBlue, int separacionTop, int separacionBottom, TextAlignment posicionLetra)
        {
            Paragraph letrasBody = new Paragraph(letras);
            letrasBody.SetTextAlignment(posicionLetra);
            letrasBody.SetFontSize(sizeLetra);
            letrasBody.SetPaddingTop(separacionTop); //separacion con bloque de arriba
            letrasBody.SetPaddingBottom(separacionBottom);
            letrasBody.SetMarginBottom(separacionBottom);
            letrasBody.SetMarginTop(1);
            if (isBlue)
                letrasBody.SetFontColor(new DeviceRgb(0, 64, 128));
            if (isBold)
                letrasBody.SetBold();
            return letrasBody;
        }

        private Cell DatosCelda(string contenidoCelda, bool isBold, bool conBorde, TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);

            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(213, 216, 220)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);

            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(0);
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if (isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if (!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
            // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            return datosCelda;
        }
    }
}