using System;
using System.Drawing;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Hosting;
using Color = iText.Kernel.Colors.Color;
using Image = iText.Layout.Element.Image;
using Path = iText.Kernel.Geom.Path;
using Rectangle = iText.Kernel.Geom.Rectangle;

namespace qrCode_itext.Reports
{
    public class ReportCertificacionF200
    {
        private IWebHostEnvironment _oHostEnvironment;
        public ReportCertificacionF200(IWebHostEnvironment oHostEnvironment)
        {
            _oHostEnvironment = oHostEnvironment;
        }
        
        #region Declaracion Cabecera

        private string titulo="FORMULARIO: 200 \nVERSIÓN: 3 \nIMPUESTO AL VALOR AGREGADO";
        private string cabecera ="(A) CABECERA DE LA DECLARACION JURADA";
        private string razonSocialTitle = " NOMBRE(S) Y APELLIDO(S) O RAZON SOCIAL DEL SUJETO PASIVO";
        private string incisoC ="B) DETERMINACION DEL SALDO DEFINITIVO A FAVOR DEL FISCO O DEL CONTRIBUYENTE";
        #endregion

        #region Declaracion Rubro 1
        private string rubro1 = "RUBRO 1) DETERMINACION DEL DEBITO FISCAL";
        private string rubro1_a = "Ventas de bienes y/o servicios gravados en el mercado interno, excepto ventas gravadas con Tasa Cero";
        private string rubro1_b = "Exportación de bienes y operaciones exentas";
        private string rubro1_c = "Ventas gravadas a Tasa Cero";
        private string rubro1_d = "Ventas no gravadas y operaciones que no son objeto del IVA";
        private string rubro1_e = "Valor atribuido a bienes y/ o servicios retirados y consumos particulares";
        private string rubro1_f = "Devoluciones y rescisiones efectuadas en el período";
        private string rubro1_g = "Descuentos, bonificaciones y rebajas obtenidos en el periodo";
        private string rubro1_h = "Débito Fiscal correspondiente a: ((C13 + C16 + C17 + C18) * 13%)";
        private string rubro1_i = "Débito Fiscal actualizado correspondiente a reintegros";
        private string rubro1_j = "Total Débito Fiscal del período (C39 + C55)";
        #endregion

        #region Declaracion Rubro 2
        private string rubro2 = "RUBRO 2) DETERMINACION DEL CREDITO FISCAL";
        private string rubro2_a = "Total compras correspondientes a actividades gravadas y/o no gravadas";
        private string rubro2_b = "Compra directamente vinculadas a actividad gravadas";
        private string rubro2_c = "Compras en las que no es posible discriminar su vinculación con actividades gravadas y no gravadas";
        private string rubro2_d = "Devoluciones y rescisiones recibidas en el período";
        private string rubro2_e = "Descuentos, bonificaciones y rebajas otorgadas en el período";
        private string rubro2_f = "Crédito Fiscal correspondiente a: ((C26 + C27 + C28) * 13%)";
        private string rubro2_g = "Crédito Fiscal proporcional correspondiente a la actividad gravada (C31*(C13+C14)/(C13+C14+C15+C505))*13%";
        private string rubro2_h = "Total Crédito Fiscal del período (C114 + C1003)";
        #endregion

        #region Declaracion Rubro 3
        private string rubro3 = "RUBRO 3) DETERMINACION DE LA DIFERENCIA";
        private string rubro3_a = "Diferencia a favor del Contribuyente (C1004 - C1002; Si >0)";
        private string rubro3_b = "Diferencia a favor del Fisco o Impuesto Determinado (C1002 - C1004; Si > 0)";
        private string rubro3_c = "Saldo de Crédito Fiscal del periodo anterior a compensar (C592 del Formulario del periodo anterior)";
        private string rubro3_d = "Actualización de valor sobre el saldo de Crédito Fiscal del período anterior";
        private string rubro3_e = "Saldo de Impuesto Determinado a favor del Fisco (C909-C635-C648;Si > 0)";
        private string rubro3_f = "Pago a cuenta del 50% de contribuciones patronales pagadas del periodo (Sobre Cotizaciones Pagadas del Periodo anterior); según D.S. 4298";
        private string rubro3_g = "Saldo a favor del Fisco despues de compensar pagos a cuenta por contribuciones patronales \n(C1001 - C621; Si > 0) ((C621 - C1001; Si > 0) = 0)";
        private string rubro3_h = "Pagos a Cuenta realizados en DD.JJ. y/o Boletas de Pago correspondientes al período que se declara";
        private string rubro3_i = "Saldo de Pagos a Cuenta del período anterior a compensar (C747 del Formulario del período anterior)";
        private string rubro3_j = "Saldo por Pagos a Cuenta a favor del Contribuyente (C640+C622-C629; Si >0)";
        private string rubro3_k = "Saldo a favor del fisco despues de compensar pagos a cuenta (C629-C622-C640; Si > 0)";
        private string rubro3_l = "Pago a cuenta del 5% por compras a contribuyentes del SIETE- RG; según D.S. 4298";
        private string rubro3_m = "Saldo de Pagos a cuenta 5% por compras a contribuyentes del SIETE- RG (C469 del periodo anterior)";
        private string rubro3_n = "Saldo a favor despues de compensar pagos a cuenta 5% por compras a contribuyentes SIETE- RG (C465+C466-C468; Si > 0)";
        private string rubro3_o = "Saldo a favor del Fisco (C468-C465-C466; Si > 0)";
        #endregion

        #region Declaracion Rubro 4
        private string rubro4 = "RUBRO 4) DETERMINACION DE LA DEUDA TRIBUTARIA";
        private string rubro4_a = "Tributo Omitido (C996)";
        private string rubro4_b = "Actualización de valor sobre Tributo Omitido";
        private string rubro4_c = "Intereses sobre Tributo Omitido Actualizado";
        private string rubro4_d = "Multa por Incumplimiento al Deber Formal (IDF) por presentación fuera de plazo";
        private string rubro4_e = "Multa por IDF por incremento del Impuesto Determinado en DDJ.JJ. Rectificatoria presentada fuera de plazo";
        private string rubro4_f = "Total Deuda Tributaria (C924+C925+C938+C954+C967)";
        #endregion

        #region Declaracion Rubro 5
        private string rubro5 = "RUBRO 5) SALDO DEFINITIVO";
        private string rubro5_a = "Saldo definitivo de Crédito Fiscal a favor del Contribuyente para el siguiente período \n(C693+C635+C648-C909;Si > 0)";
        private string rubro5_b = "Saldo definitivo por Pagos a Cuenta a favor del Contribuyente del 5% por compras a contribuyentes del SIETE-RG para el siguiente período (C467); Si>0";
        private string rubro5_c = "Saldo definitivo por Pagos a Cuenta a favor del Contribuyente para el siguiente periodo (C643 - C955); Si > 0";
        private string rubro5_d = "Saldo definitivo a favor del Fisco (C996 ó (C955-C643) según corresponda; Si >0)";
        #endregion

        #region Declaracion Rubro 6
        private string rubro6 = "RUBRO 6) IMPORTE DE PAGO";
        private string rubro6_a = "Pago en Valores (Sujeto a Verificación y Confirmación por el SIN)";
        private string rubro6_b = "Pago en Efectivo (C646 - C677; Si >0)";
        #endregion

        #region Declaracion Rubro 7
        private string rubro7 = "RUBRO 7) DATOS INFORMATIVOS";
        private string rubro7_a = "Permuta en venta de bienes y/o servicios";
        private string rubro7_b = "Permuta en compra de bienes y/o servicios";
        #endregion

        private string firmaDeclaracion = "JURO LA EXACTITUD DE LA PRESENTE DECLARACION (Artículo N° 22 y Artículo N°78 Parrafo I de la Ley N°2492 Código Tributario Boliviano)" +
                                          "\n \n \n \t -------------------------------------------------------------" +
                                          "\n \t \t Firma del sujetopasivo o tercero responsable";

        private string footer = "Impresion con validez probatoria conforme al art. 79 de la Ley N° 2492 y 3er. Párrafo del art. 7 del D.S. N° 27310";

        /*private ReporteNumTramiteDto _datosReporte = new ReporteNumTramiteDto();*/

        public byte[] Report(byte[] dto)
        {
            /*_datosReporte = datosReporte;*/
            
            
            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.LETTER);
            document.SetMargins(30f,55f,40f,55f);
            
            // document.Add(new Paragraph("Hello world!"));
            
            //LOGO
            string path = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine = System.IO.Path.Combine(path, "sin.png");
            var logo = new Image(ImageDataFactory.Create(imgCombine));
            logo.Scale(3,3);
            // document.Add(logo);
            var qrData = dto;
            Image qrImage = new Image(ImageDataFactory.Create(qrData));

            //Tabla HEADER
            float[] cellWidth0 = {25f, 63f, 12f};
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            Cell imgCell = new Cell().Add(logo.SetAutoScale(true));  //cell para el LOGO
            imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0);
            Cell imgCellQr = new Cell().Add(qrImage.SetWidth(70)); //cell para el QR
            imgCellQr.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0);
            table0.SetFontSize(11);
            
            table0.AddCell(imgCell);
            table0.AddCell(DatosCelda(titulo,false,false,TextAlignment.LEFT).SetPadding(5));
            table0.AddCell(imgCellQr);

            document.Add(table0);
            
            //cabecera
            
            #region TABLA CABECERA
            // Table cabecera
            document.Add(LetrasBody(cabecera,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth1 = {30f, 10f, 10f, 30f, 20f};
            Table table1 = new Table(UnitValue.CreatePercentArray(cellWidth1)).UseAllAvailableWidth();
            table1.SetFontSize(7);
            
            Cell razonSocial = new Cell(1,5).Add(new Paragraph(razonSocialTitle)); //cell para el QR
            table1.AddCell(razonSocial.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            Cell razonSocialData = new Cell(1,5).Add(new Paragraph("cabecera")); //cell para el QR
            table1.AddCell(razonSocialData.SetMargin(0).SetPadding(0).SetTextAlignment(TextAlignment.CENTER));

            table1.AddCell(DatosCelda(" NIT",false,true,TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            Cell periodoCell = new Cell(1,2).Add(new Paragraph(" PERIODO").SetTextAlignment(TextAlignment.LEFT)); //cell para el QR
            table1.AddCell(periodoCell.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            table1.AddCell(DatosCelda(" NÚMERO DE ORDEN",false,true,TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table1.AddCell(DatosCelda(" DD.JJ. ORIGINAL",false,true,TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            
            table1.AddCell(DatosCelda("8302579845",false,true,TextAlignment.CENTER));
            table1.AddCell(DatosCelda("Mes: " + "12",false,true,TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Año: "+"2020",false,true,TextAlignment.LEFT));
            table1.AddCell(DatosCelda("456789132",false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table1.AddCell(DatosCelda("Cod. 534: x",false,true,TextAlignment.LEFT));

            document.Add(table1);
            #endregion

            //Insiso C
            document.Add(LetrasBody(incisoC,8,false,true,0,0,TextAlignment.LEFT));
            // Table Rubro1
            #region RUBRO1
            document.Add(LetrasBody(rubro1,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth2 = {4f, 70f, 6f, 20f};
            Table table2 = new Table(UnitValue.CreatePercentArray(cellWidth2)).UseAllAvailableWidth();
            table2.SetFontSize(7);
            
            table2.AddCell(DatosCelda("Fila",false,true,TextAlignment.CENTER).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("DETERMINACIÓN DE LA BASE IMPONIBLE E IMPUESTO",false,true,TextAlignment.LEFT).SetPaddingLeft(3).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("Cód.",false,true,TextAlignment.CENTER).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table2.AddCell(DatosCelda("IMPORTE (EN BOLIVIANOS)",false,true,TextAlignment.RIGHT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));

            table2.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("13",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("14",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("c",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_c,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("15",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("d",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_d,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("505",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("e",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_e,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("16",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("f",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_f,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("17",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("g",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_g,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("18",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("h",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_h,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("39",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("i",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_i,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("55",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table2.AddCell(DatosCelda("j",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda(rubro1_j,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table2.AddCell(DatosCelda("1002",false,true,TextAlignment.CENTER));
            table2.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            document.Add(table2);
            #endregion
            // Table Rubro2
            #region RUBRO2
            document.Add(LetrasBody(rubro2,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth3 = {4f, 70f, 6f, 20f};
            Table table3 = new Table(UnitValue.CreatePercentArray(cellWidth3)).UseAllAvailableWidth();
            table3.SetFontSize(7);

            table3.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("11",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("26",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("c",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_c,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("31",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("d",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_d,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("27",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("e",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_e,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("28",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("f",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_f,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("114",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("g",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_g,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("1003",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda("h",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda(rubro2_h,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("1004",false,true,TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));

            document.Add(table3);
            #endregion
            // Table Rubro3
            #region RUBRO3
            document.Add(LetrasBody(rubro3,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth4 = {4f, 70f, 6f, 20f};
            Table table4 = new Table(UnitValue.CreatePercentArray(cellWidth4)).UseAllAvailableWidth();
            table4.SetFontSize(7);

            table4.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("693",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("909",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("c",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_c,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("635",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("d",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_d,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("648",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("e",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_e,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("1001",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("f",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_f,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("621",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("g",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_g,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("629",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("h",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_h,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("622",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("i",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_i,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("640",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("j",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_j,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("643",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("k",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_k,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("468",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("l",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_l,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("465",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("m",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_m,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("466",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("n",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_n,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("467",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table4.AddCell(DatosCelda("o",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda(rubro3_o,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table4.AddCell(DatosCelda("966",false,true,TextAlignment.CENTER));
            table4.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            document.Add(table4);
            #endregion
            // Table Rubro4
            #region RUBRO4
            document.Add(LetrasBody(rubro4,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth5 = {4f, 70f, 6f, 20f};
            Table table5 = new Table(UnitValue.CreatePercentArray(cellWidth5)).UseAllAvailableWidth();
            table5.SetFontSize(7);

            table5.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("924",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table5.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("925",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table5.AddCell(DatosCelda("c",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_c,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("938",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table5.AddCell(DatosCelda("d",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_d,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("954",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table5.AddCell(DatosCelda("e",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_e,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("967",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table5.AddCell(DatosCelda("f",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda(rubro4_f,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table5.AddCell(DatosCelda("955",false,true,TextAlignment.CENTER));
            table5.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            document.Add(table5);
            #endregion
            // Table Rubro5
            #region RUBRO5
            document.Add(LetrasBody(rubro5,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth6 = {4f, 70f, 6f, 20f};
            Table table6 = new Table(UnitValue.CreatePercentArray(cellWidth6)).UseAllAvailableWidth();
            table6.SetFontSize(7);

            table6.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("592",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table6.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("469",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table6.AddCell(DatosCelda("c",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_c,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("747",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table6.AddCell(DatosCelda("d",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda(rubro5_d,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table6.AddCell(DatosCelda("646",false,true,TextAlignment.CENTER));
            table6.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));

            document.Add(table6);
            #endregion
            // Table Rubro6
            #region RUBRO6
            document.Add(LetrasBody(rubro6,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth7 = {4f, 70f, 6f, 20f};
            Table table7 = new Table(UnitValue.CreatePercentArray(cellWidth7)).UseAllAvailableWidth();
            table7.SetFontSize(7);

            table7.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table7.AddCell(DatosCelda(rubro6_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table7.AddCell(DatosCelda("677",false,true,TextAlignment.CENTER));
            table7.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table7.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table7.AddCell(DatosCelda(rubro6_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table7.AddCell(DatosCelda("576",false,true,TextAlignment.CENTER));
            table7.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));

            document.Add(table7);
            #endregion
            // Table Rubro7
            #region RUBRO7
            document.Add(LetrasBody(rubro7,8,false,true,0,0,TextAlignment.LEFT));
            
            float[] cellWidth8 = {4f, 70f, 6f, 20f};
            Table table8 = new Table(UnitValue.CreatePercentArray(cellWidth8)).UseAllAvailableWidth();
            table8.SetFontSize(7);

            table8.AddCell(DatosCelda("a",false,true,TextAlignment.CENTER));
            table8.AddCell(DatosCelda(rubro7_a,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table8.AddCell(DatosCelda("580",false,true,TextAlignment.CENTER));
            table8.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));
            
            table8.AddCell(DatosCelda("b",false,true,TextAlignment.CENTER));
            table8.AddCell(DatosCelda(rubro7_b,false,true,TextAlignment.LEFT).SetPaddingLeft(3));
            table8.AddCell(DatosCelda("581",false,true,TextAlignment.CENTER));
            table8.AddCell(DatosCelda("1111111",false,true,TextAlignment.RIGHT));

            document.Add(table8);
            #endregion
            // Table Firma
            #region FIRMA Y CI

            document.Add(new Paragraph(" "));
                
            float[] cellWidth9 = {50f, 50f};
            Table table9 = new Table(UnitValue.CreatePercentArray(cellWidth9)).UseAllAvailableWidth();
            Cell firmaCell = new Cell(2,1).Add(new Paragraph(firmaDeclaracion).SetTextAlignment(TextAlignment.CENTER)); 
            firmaCell.SetPadding(0).SetMargin(0);
            table9.SetFontSize(7);
            
            table9.AddCell(firmaCell);
            table9.AddCell(DatosCelda("Aclaración de Firma: \n \n \n \n",false,true,TextAlignment.LEFT));
            table9.AddCell(DatosCelda("C.I.:",false,true,TextAlignment.LEFT).SetVerticalAlignment(VerticalAlignment.TOP));

            document.Add(table9);
            #endregion
            
            //footer
            document.Add(LetrasBody(footer, 8, false, false, 2, 2, TextAlignment.LEFT));
            //Numeero de certificacion, Cod seguridad y Fecha
            #region Num Certificacion, Seguridad y Fecha
            float[] cellWidth10 = {34f, 33f, 33f};
            Table table10 = new Table(UnitValue.CreatePercentArray(cellWidth10)).UseAllAvailableWidth();
            table10.SetFontSize(7);
            
            table10.AddCell(DatosCelda("Código de Certificación: " + "123456",false,false,TextAlignment.LEFT));
            table10.AddCell(DatosCelda("Código de Seguridad: "+"798465",false,false,TextAlignment.CENTER));
            table10.AddCell(DatosCelda("Fecha de Certificación: "+ DateTime.Now.ToString("dd/MM/yyyy"),false,false,TextAlignment.RIGHT));

            document.Add(table10);
            #endregion
            
            document.Close();

            return stream.ToArray();
        }
        
        private Paragraph LetrasBody(string letras, int sizeLetra, bool isBold,bool isBlue, int separacionTop, int separacionBottom, TextAlignment posicionLetra)
        {
            Paragraph letrasBody = new Paragraph(letras);
            letrasBody.SetTextAlignment(posicionLetra);
            letrasBody.SetFontSize(sizeLetra);
            letrasBody.SetPaddingTop(separacionTop); //separacion con bloque de arriba
            letrasBody.SetPaddingBottom(separacionBottom);
            letrasBody.SetMarginBottom(separacionBottom);
            letrasBody.SetMarginTop(1);
            if(isBlue)
                letrasBody.SetFontColor(new DeviceRgb(0, 64, 128));
            if(isBold)
                letrasBody.SetBold();
            return letrasBody;
        }
        
        private Cell DatosCelda(string contenidoCelda, bool isBold,bool conBorde,TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);
            
            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(  213, 216, 220)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);
            
            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(0);   
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if(isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if(!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
                // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));
            

            return datosCelda;
        }
    }
}