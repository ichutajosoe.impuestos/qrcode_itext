﻿using System;
using System.Collections.Generic;
using iText.IO.Image;
using System.Linq;
using System.Threading.Tasks;
using iText.Kernel.Events;
using iText.Kernel.Pdf;
using iText.Kernel.Geom;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Layout.Borders;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Canvas;

namespace qrCode_itext.Reportes
{
    public class HeaderEventHandler : IEventHandler
    {
        string _dirImg;
        public HeaderEventHandler(string dirImg)
        {
            _dirImg = dirImg;
        }

        public void HandleEvent(Event @event)
        {
            PdfDocumentEvent docEvent = (PdfDocumentEvent)@event;
            PdfDocument pdfDoc = docEvent.GetDocument();
            PdfPage page = docEvent.GetPage();

            //Una mejor forma de añadir el Canvas
            PdfCanvas canvas1 = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), pdfDoc);
            Rectangle rootArea = new Rectangle(35, page.GetPageSize().GetTop() - 70, page.GetPageSize().GetRight() - 70, 50);
            new Canvas(canvas1, rootArea).Add(GetHeader(docEvent));

            ////Esta es otra forma de Añadir la cabecera (Funciona lo probe)
            //Rectangle rootArea = new Rectangle(35, page.GetPageSize().GetTop()-70,page.GetPageSize().GetRight()-70,50);
            //Canvas canvas = new Canvas(docEvent.GetPage(), rootArea);
            ////Canvas canvas = new Canvas(page, rootArea);   //TAmbien funciona ASI
            //canvas
            //    .Add(GetHeader(docEvent))
            //    .ShowTextAligned("Este es el encabezado de Pagina", 10, 0, iText.Layout.Properties.TextAlignment.CENTER)
            //    .Close();
        }

        public Table GetHeader(PdfDocumentEvent docEvent)
        {
            //header
            #region HEADER
            //LOGO

            var logo = new Image(ImageDataFactory.Create(_dirImg));
            logo.Scale(3, 3);

            //Tabla HEADER
            float[] cellWidth0 = { 80f, 20f };
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            // table0.SetFontSize(11);

            Cell tituloCell = new Cell().Add(new Paragraph("Extracto Tributario").SetTextAlignment(TextAlignment.LEFT).SetFontSize(14).SetPaddingTop(10));
            table0.AddCell(tituloCell.SetMargin(0).SetPaddingLeft(3).SetBold().SetBorder(Border.NO_BORDER));

            Cell imgCell = new Cell(2, 1).Add(logo.SetAutoScale(true));  //cell para el LOGO
            table0.AddCell(imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0));

            table0.AddCell(DatosCelda("R-0436", false, false, TextAlignment.LEFT).SetFontSize(8));

            return table0;
            #endregion
        }
        private Cell DatosCelda(string contenidoCelda, bool isBold, bool conBorde, TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);

            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(229, 231, 233)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);

            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(1);
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            if (posicionLetra == TextAlignment.LEFT)
                datosCelda.SetPaddingLeft(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if (isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if (!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
            // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            return datosCelda;
        }
    }
}
