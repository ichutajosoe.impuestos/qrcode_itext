using System;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Hosting;

namespace qrCode_itext.Reports
{
    public class CertificadoBoleta1000
    {
        private IWebHostEnvironment _oHostEnvironment;
        public CertificadoBoleta1000(IWebHostEnvironment oHostEnvironment)
        {
            _oHostEnvironment = oHostEnvironment;
        }
        
        #region Declaracion Cabecera

        private string titulo = "BOLETA DE PAGO 1000v2";
        private string cabecera = "(A) CABECERA DE LA BOLETA DE PAGO";
        private string razonSocialTitle = "NOMBRE(S) Y APELLIDO(S) O RAZON SOCIAL DEL SUJETO PASIVO";
        private string incisoB = "B) DETERMINACION DEL SALDO DEFINITIVO A FAVOR DEL FISCO O DEL CONTRIBUYENTE";
        #endregion

        #region rubro 1
        private string rubro1_1 = "Importe de la deuda";
        private string rubro1_2 = "Mantenimiento de Valor";
        private string rubro1_3 = "Intereses Moratorios";
        private string rubro1_4 = "Multa por Incumplimiento a los Deberes Formales";
        private string rubro1_5 = "Multas por Incremento del Impuesto Determinado en la Declaración Jurada Rectificativa";
        private string rubro1_6 = "Sanción por Omisión al Pago";
        private string rubro1_7 = "Total (C909 + C925 + C938 + C954 + C967 + C942)";
        private string rubro1_8 = "Saldo Disponible de Pagos a Compensar";
        private string rubro1_9 = "Pago en Efectivo (C996 - C640)";
        #endregion

        private string rubro8 = "RUBRO 8 - DATOS INFORMATIVOS DE PAGO MEDIANTE SIGMA (SOLO SECTOR PUBLICO)";
        private string firmaDeclaracion = "JURO LA EXACTITUD DE LA PRESENTE DECLARACION (Artículo N° 22 y Artículo N°78 Parrafo I de la Ley N°2492 Código Tributario Boliviano)" +
                                          "\n \n \n \t -------------------------------------------------------------" +
                                          "\n \t \t Firma del sujeto pasivo o tercero responsable";

        private string footer = "Impresion con validez probatoria conforme al art. 79 de la Ley N° 2492 y 3er. Párrafo del art. 7 del D.S. N° 27310";

        
        public byte[] Report(byte[] dto)
        {
            /*_datosReporte = datosReporte;*/
            
            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.LETTER);
            document.SetMargins(30f, 55f, 40f, 55f);

            // document.Add(new Paragraph("Hello world!"));
            
            //header
            #region HEADER
            //LOGO
            string path = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine = System.IO.Path.Combine(path, "sin.png");
            var logo = new Image(ImageDataFactory.Create(imgCombine));
            logo.Scale(3, 3);
            // document.Add(logo);
            // var qrData = dto;
            // Image qrImage = new Image(ImageDataFactory.Create(qrData));
            string path1 = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine1 = System.IO.Path.Combine(path1, "qrPicture.png");
            var logo1 = new Image(ImageDataFactory.Create(imgCombine1));

            //Tabla HEADER
            float[] cellWidth0 = { 30f, 58f, 12f };
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            
            Cell imgCell = new Cell(3,1).Add(logo.SetAutoScale(true));  //cell para el LOGO
            table0.AddCell(imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0));
            
            Cell tituloCell = new Cell().Add(new Paragraph(titulo).SetTextAlignment(TextAlignment.LEFT).SetFontSize(11));   //titulo
            table0.AddCell(tituloCell.SetMargin(0).SetPadding(0).SetBold().SetBorder(Border.NO_BORDER));
            
            Cell imgCellQr = new Cell(3,1).Add(logo1.SetWidth(70)); //cell para el QR
            table0.AddCell(imgCellQr.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0));
            
            Cell Cellsubtitulo = new Cell().Add(new Paragraph("PAGO EN EFECTIVO").SetTextAlignment(TextAlignment.LEFT).SetFontSize(11));    //subtitulo
            table0.AddCell(Cellsubtitulo.SetMargin(0).SetPadding(0).SetBold().SetBorder(Border.NO_BORDER));
            table0.AddCell(DatosCelda("\nNÚMERO DE ORDEN: " + "7984655", false, false, TextAlignment.LEFT).SetFontSize(8));

            document.Add(table0);
            #endregion

            //cabecera 1
            #region TABLA CABECERA
            // Table cabecera
            document.Add(LetrasBody(cabecera, 8, false, true, 0, 0, TextAlignment.LEFT));
            //cabecera de la razon Social
            float[] cellWidth01 = { 100f};
            Table table01 = new Table(UnitValue.CreatePercentArray(cellWidth01)).UseAllAvailableWidth();
            table01.SetFontSize(7);
            table01.AddCell(DatosCelda(razonSocialTitle, false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table01.AddCell(DatosCelda("cabecera", false, true, TextAlignment.CENTER));
            document.Add(table01);
            
            //Separacion
            document.Add(new Paragraph(" ").SetMargins(3,0,0,0));
            
            float[] cellWidth1 = { 30f, 10f, 10f, 50f };
            Table table1 = new Table(UnitValue.CreatePercentArray(cellWidth1)).UseAllAvailableWidth();
            table1.SetFontSize(7);

            table1.AddCell(DatosCelda(" NIT", false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            Cell periodoCell = new Cell(1, 2).Add(new Paragraph(" PERIODO").SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); //cell para el QR
            table1.AddCell(periodoCell.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            table1.AddCell(DatosCelda("FOLIO", false, true, TextAlignment.LEFT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));

            table1.AddCell(DatosCelda("8302579845", false, true, TextAlignment.CENTER));
            table1.AddCell(DatosCelda("Mes: " + "12", false, true, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("Año: " + "2020", false, true, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("USO ENTIDAD FINANCIERA o COLECTURIA 0", false, true, TextAlignment.LEFT));

            document.Add(table1);
            #endregion
            
            document.Add(new Paragraph(" ").SetMargins(3,0,0,0));
            //cabecera 2
            #region cabecera2
            float[] cellWidth2 = { 8f,7f,8f,7f,8f,12f,8f,12f,8f,7f,8f,7f };
            Table table2 = new Table(UnitValue.CreatePercentArray(cellWidth2)).UseAllAvailableWidth();
            table2.SetFontSize(7);

            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Operación",false)),true));
            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Código de Impuesto",false)),true));
            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Código de Formulario que Paga",false)),true));
            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Número de Orden del Documento que Paga",false)),true));
            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Cargo",false)),true));
            table2.AddCell(PropiedadesCelda(new Cell(1,2).Add(LetrasCabeceraTabla("Número de Cuota",false)),true));

            table2.AddCell(DatosCelda("Cód. 001", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("13", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("Cód. 008", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("30", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("Cód. 017", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("200", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("Cód. 012",false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("3698745", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("Cód. 018", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("0", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("Cód. 014", false, true, TextAlignment.LEFT));
            table2.AddCell(DatosCelda("0", false, true, TextAlignment.LEFT));

            document.Add(table2);
            #endregion
            
            document.Add(new Paragraph(" ").SetMargins(3,0,0,0));
            
            // Table Rubro1
            #region RUBRO1
            document.Add(LetrasBody("(B) DETALLE DE LA BOLETA DE PAGO", 8, false, true, 0, 0, TextAlignment.LEFT));

            float[] cellWidth3 = { 74f, 6f, 20f };
            Table table3 = new Table(UnitValue.CreatePercentArray(cellWidth3)).UseAllAvailableWidth();
            table3.SetFontSize(7);
            
            table3.AddCell(DatosCelda("DESCRIPCIÓN", false, true, TextAlignment.LEFT).SetPaddingLeft(3).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table3.AddCell(DatosCelda("Cód.", false, true, TextAlignment.CENTER).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            table3.AddCell(DatosCelda("IMPORTE (EN BOLIVIANOS)", false, true, TextAlignment.RIGHT).SetBackgroundColor(ColorConstants.LIGHT_GRAY));
            
            table3.AddCell(DatosCelda(rubro1_1, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("909", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_2, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("925", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_3, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("938", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_4, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("954", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_5, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("967", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_6, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("942", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_7, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("996", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_8, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("640", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));
            
            table3.AddCell(DatosCelda(rubro1_9, false, true, TextAlignment.LEFT).SetPaddingLeft(3));
            table3.AddCell(DatosCelda("576", false, true, TextAlignment.CENTER));
            table3.AddCell(DatosCelda("1111111", false, true, TextAlignment.RIGHT));

            document.Add(table3);
            #endregion

            #region Datos SIGMA
            document.Add(new Paragraph(" ").SetMargins(3,0,0,0));
            
            float[] cellWidth4 = { 8f,17f,8f,17f,8f,17f,8f,17f };
            Table table4 = new Table(UnitValue.CreatePercentArray(cellWidth4)).UseAllAvailableWidth();
            table4.SetFontSize(7);
            
            Cell CellRubro8 = new Cell(1, 8).Add(new Paragraph(rubro8).SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); 
            table4.AddCell(CellRubro8.SetMargin(0).SetPadding(0));
            
            Cell CellC31 = new Cell(1, 2).Add(new Paragraph("N° C31").SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); 
            table4.AddCell(CellC31.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            Cell CellNumPago = new Cell(1, 2).Add(new Paragraph("N° de pago").SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); 
            table4.AddCell(CellNumPago.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            Cell CellFechaPago = new Cell(1, 2).Add(new Paragraph("Fecha confirmación de pago").SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); 
            table4.AddCell(CellFechaPago.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            Cell CellSigma = new Cell(1, 2).Add(new Paragraph("Importe pagado vía SIGMA").SetTextAlignment(TextAlignment.LEFT).SetPaddingLeft(3)); 
            table4.AddCell(CellSigma.SetMargin(0).SetBackgroundColor(ColorConstants.LIGHT_GRAY).SetPadding(0));
            
            table4.AddCell(DatosCelda("Cód. 8880" , false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda(" - " , false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda("Cód. 8882", false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda(" - " , false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda("Cód. 8881", false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda(" - " , false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda("Cód. 8883", false, true, TextAlignment.LEFT));
            table4.AddCell(DatosCelda(" - " , false, true, TextAlignment.LEFT));

            document.Add(table4);
            #endregion

            // Table Firma
            #region FIRMA Y CI

            document.Add(new Paragraph(" ").SetMargins(5,0,0,0));

            float[] cellWidth9 = { 50f, 50f };
            Table table9 = new Table(UnitValue.CreatePercentArray(cellWidth9)).UseAllAvailableWidth();
            Cell firmaCell = new Cell(2, 1).Add(new Paragraph(firmaDeclaracion).SetTextAlignment(TextAlignment.CENTER));
            firmaCell.SetPadding(0).SetMargin(0);
            table9.SetFontSize(7);

            table9.AddCell(firmaCell);
            table9.AddCell(DatosCelda("Aclaración de Firma: \n \n \n \n", false, true, TextAlignment.LEFT));
            table9.AddCell(DatosCelda("C.I.:", false, true, TextAlignment.LEFT).SetVerticalAlignment(VerticalAlignment.TOP));

            document.Add(table9);
            #endregion
            
            #region ENTIDAD FINANCIERA
            document.Add(LetrasBody("(C) REFRENDO DE LA ENTIDAD FINANCIERA", 8, false, true, 0, 0, TextAlignment.LEFT));
            float[] cellWidth09 = { 100f};
            Table table09 = new Table(UnitValue.CreatePercentArray(cellWidth09)).UseAllAvailableWidth();
            table09.SetFontSize(7);
            
            table09.AddCell(DatosCelda("Datos de la Entidad Bancaria\n" +
                                       "Formulario\n" +
                                       "N° Orden\n" +
                                       "NIT \n" +
                                       "Periodo", false, true, TextAlignment.LEFT));
            document.Add(table09);
            #endregion

            //footer
            document.Add(LetrasBody(footer, 8, false, false, 2, 2, TextAlignment.LEFT));
            //Numeero de certificacion, Cod seguridad y Fecha
            #region Num Certificacion, Seguridad y Fecha
            float[] cellWidth10 = { 50f, 50f};
            Table table10 = new Table(UnitValue.CreatePercentArray(cellWidth10)).UseAllAvailableWidth();
            table10.SetFontSize(7);

            table10.AddCell(DatosCelda("Código de Certificación: " + "123456", false, false, TextAlignment.LEFT));
            table10.AddCell(DatosCelda("Fecha de Certificación: " + DateTime.Now.ToString("dd/MM/yyyy"), false, false, TextAlignment.RIGHT));

            document.Add(table10);
            document.Add(LetrasBody("Código de Seguridad: " + "798465", 7, false, false, 2, 2, TextAlignment.LEFT).SetPaddingLeft(3));
            #endregion

            document.Close();

            return stream.ToArray();
        }

        private Paragraph LetrasBody(string letras, int sizeLetra, bool isBold, bool isBlue, int separacionTop, int separacionBottom, TextAlignment posicionLetra)
        {
            Paragraph letrasBody = new Paragraph(letras);
            letrasBody.SetTextAlignment(posicionLetra);
            letrasBody.SetFontSize(sizeLetra);
            letrasBody.SetPaddingTop(separacionTop); //separacion con bloque de arriba
            letrasBody.SetPaddingBottom(separacionBottom);
            letrasBody.SetMarginBottom(separacionBottom);
            letrasBody.SetMarginTop(1);
            if (isBlue)
                letrasBody.SetFontColor(new DeviceRgb(0, 64, 128));
            if (isBold)
                letrasBody.SetBold();
            return letrasBody;
        }
        private Paragraph LetrasCabeceraTabla(string letras, bool isBold)
        {
            Paragraph text = new Paragraph(letras);
            text.SetTextAlignment(TextAlignment.LEFT);
            text.SetPaddingLeft(3);
            if(isBold)
                text.SetBold();
            return text;
        }

        private Cell PropiedadesCelda(Cell celda, bool isBgGray)
        {
            celda.SetMargin(0);
            celda.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            if (isBgGray)
            {
                celda.SetPadding(0);
                celda.SetBackgroundColor(ColorConstants.LIGHT_GRAY);
            }
            else
            {
                celda.SetPadding(2);
            }
            return celda;
        }
        private Cell DatosCelda(string contenidoCelda, bool isBold, bool conBorde, TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);

            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(213, 216, 220)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);

            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(0);
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            if (posicionLetra == TextAlignment.LEFT)
                datosCelda.SetPaddingLeft(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if (isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if (!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
            // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            return datosCelda;
        }
    }
}