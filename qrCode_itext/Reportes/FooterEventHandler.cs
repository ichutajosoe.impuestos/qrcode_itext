﻿using iText.Kernel.Colors;
using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qrCode_itext.Reportes
{
    public class FooterEventHandler : IEventHandler
    {
        public void HandleEvent(Event @event)
        {
            PdfDocumentEvent docEvent = (PdfDocumentEvent)@event;
            PdfDocument pdfDoc = docEvent.GetDocument();
            PdfPage page = docEvent.GetPage();

            //Una mejor forma de añadir el Canvas
            PdfCanvas canvas1 = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), pdfDoc);
            Rectangle rootArea = new Rectangle(36, 20, page.GetPageSize().GetRight() - 70, 50);
            new Canvas(canvas1, rootArea).Add(GetFooter(docEvent));
        }
        public Table GetFooter(PdfDocumentEvent docEvent)
        {

            #region FOOTER
            //Tabla HEADER
            float[] cellWidth0 = { 92f, 8f };
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();

            PdfPage page = docEvent.GetPage();
            int pageNum = docEvent.GetDocument().GetPageNumber(page);

            Style styleCell = new Style()
                .SetPadding(5)
                .SetBorder(iText.Layout.Borders.Border.NO_BORDER)
                .SetBorderTop(new SolidBorder(ColorConstants.BLACK, 2));

            Cell cell = new Cell().Add(new Paragraph(" "));

            table0.AddCell(cell.AddStyle(styleCell).SetTextAlignment(TextAlignment.RIGHT)
                .SetFontColor(ColorConstants.LIGHT_GRAY));

            cell = new Cell().Add(new Paragraph(pageNum.ToString()));
            table0.AddCell(cell);

            return table0;
            #endregion
        }
    }
}
