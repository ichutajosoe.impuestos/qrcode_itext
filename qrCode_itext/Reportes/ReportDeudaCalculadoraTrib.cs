using System;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Hosting;

namespace qrCode_itext.Reports
{
    public class ReportDeudaCalculadoraTrib
    {
        private IWebHostEnvironment _oHostEnvironment;
        public ReportDeudaCalculadoraTrib(IWebHostEnvironment oHostEnvironment)
        {
            _oHostEnvironment = oHostEnvironment;
        }
        
        #region Declaracion Cabecera
        private string titulo = "CÁLCULO DE DEUDA TRIBUTARIA";
        
        #endregion
        
        
        public byte[] Report(byte[] dto)
        {
            /*_datosReporte = datosReporte;*/
            
            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.LETTER.Rotate());
            document.SetMargins(30f, 55f, 40f, 55f);

            // document.Add(new Paragraph("Hello world!"));
            
            //header
            #region HEADER
            //LOGO
            string path = _oHostEnvironment.WebRootPath + "/images";
            string imgCombine = System.IO.Path.Combine(path, "sin.png");
            var logo = new Image(ImageDataFactory.Create(imgCombine));
            logo.Scale(3, 3);

            //Tabla HEADER
            float[] cellWidth0 = { 15f, 52f, 13f, 20f };
            Table table0 = new Table(UnitValue.CreatePercentArray(cellWidth0)).UseAllAvailableWidth();
            // table0.SetFontSize(11);
            
            Cell imgCell = new Cell(3,1).Add(logo.SetAutoScale(true));  //cell para el LOGO
            table0.AddCell(imgCell.SetBorder(Border.NO_BORDER).SetPadding(0).SetMargin(0));
            Cell tituloCell = new Cell(2, 1).Add(new Paragraph(titulo).SetTextAlignment(TextAlignment.CENTER).SetFontSize(13).SetPaddingTop(10)); //cell para el QR
            table0.AddCell(tituloCell.SetMargin(0).SetPadding(0).SetBold().SetBorder(Border.NO_BORDER));
            table0.AddCell(DatosCelda("Fecha de Emisión:", false, false, TextAlignment.RIGHT).SetFontSize(8));
            table0.AddCell(DatosCelda(DateTime.Now.ToString("dd/MM/yy hh:mm"), false, false, TextAlignment.LEFT).SetFontSize(8));
            table0.AddCell(DatosCelda("Usuario:", false, false, TextAlignment.RIGHT).SetFontSize(8));
            table0.AddCell(DatosCelda("Tania Mamani", false, false, TextAlignment.LEFT).SetFontSize(8));
            table0.AddCell(DatosCelda("R-0000", false, false, TextAlignment.CENTER).SetFontSize(8));

            document.Add(table0);
            #endregion
            
            //Separacion
            document.Add(new Paragraph(" ").SetMargins(7,0,0,0));
            //cabecera Datos
            #region CABECERA DATOS
            float[] cellWidth1 = { 40f, 60f };
            Table table1 = new Table(UnitValue.CreatePercentArray(cellWidth1)).UseAllAvailableWidth();
            table1.SetFontSize(9);
            
            table1.AddCell(DatosCelda("NIT:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda("4091683012", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("NOMBRE O RAZON SOCIAL:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda("ARANIBAR VARGAS JOSE ALBERTO", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("N° ORDEN:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda("19990201427", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("N° DE DOCUMENTO:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda("291629000011", false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("FECHA DE LIQUIDACIÓN:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda(DateTime.Now.ToString("dd/MM/yyyy"), false, false, TextAlignment.LEFT));
            table1.AddCell(DatosCelda("PORCENTAJE SANCIÓN:", true, false, TextAlignment.RIGHT));
            table1.AddCell(DatosCelda("0%", false, false, TextAlignment.LEFT));
            
            document.Add(table1);
            #endregion

            //Cabecera tabla
            #region CABECERA TABLA
            //Separacion
            document.Add(new Paragraph(" ").SetMargins(5,0,0,0));
            
            float[] cellWidth2 = { 6f,4f,4f,8f,8f,8f,8f,7f,7f,8f,8f,8f,8f,8f,8f };
            Table table2 = new Table(UnitValue.CreatePercentArray(cellWidth2)).UseAllAvailableWidth();
            table2.SetFontSize(6);
            
            Cell CellImpuesto = new Cell(2, 1).Add(LetrasCabeceraTabla("IMPUESTO")); 
            table2.AddCell(PropiedadesCelda(CellImpuesto));
            Cell CellPeriodoFiscal = new Cell(1, 2).Add(LetrasCabeceraTabla("PERIODO FISCAL"));
            table2.AddCell(PropiedadesCelda(CellPeriodoFiscal));
            
            Cell CellFechaVto = new Cell(2, 1).Add(LetrasCabeceraTabla("FECHA DE VTO."));
            table2.AddCell(PropiedadesCelda(CellFechaVto));
            Cell CellFechaVtoUfv = new Cell(2, 1).Add(LetrasCabeceraTabla("U.F.V (Fecha Vto.)"));
            table2.AddCell(PropiedadesCelda(CellFechaVtoUfv));
            Cell CellFechaPagoUfv = new Cell(2, 1).Add(LetrasCabeceraTabla("U.F.V (Fecha de pago)"));
            table2.AddCell(PropiedadesCelda(CellFechaPagoUfv));
            Cell CellBaseImponible = new Cell(2, 1).Add(LetrasCabeceraTabla("BASE IMPONIBLE"));
            table2.AddCell(PropiedadesCelda(CellBaseImponible));
            
            Cell CellTributoOmitido = new Cell(1, 2).Add(LetrasCabeceraTabla("TRUBUTO OMITIDO ACTUALIZADO"));
            table2.AddCell(PropiedadesCelda(CellTributoOmitido));
            Cell CellSancionConducta = new Cell(2, 1).Add(LetrasCabeceraTabla("SANCIÓN POR LA CONDUCTA (Bs.)"));
            table2.AddCell(PropiedadesCelda(CellSancionConducta));
            Cell CellTotalDeudaBs = new Cell(2, 1).Add(LetrasCabeceraTabla("TOTAL DEUDA TRIBUTARIA EN (Bs)"));
            table2.AddCell(PropiedadesCelda(CellTotalDeudaBs));
            
            Cell CellExpresadoUfv = new Cell(1, 4).Add(LetrasCabeceraTabla("EXPRESADO EN UFV"));
            table2.AddCell(PropiedadesCelda(CellExpresadoUfv));
            
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("MES"))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("AÑO"))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("TRIBUTO OMITIDO"))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("MONTO VALOR"))));
            
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("TRIBUTO OMITIDO ACT."))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("INTERÉS"))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("SANCIÓN POR LA CONDUCTA"))));
            table2.AddCell(PropiedadesCelda(new Cell().Add(LetrasCabeceraTabla("TOTAL DEUDA TRIBUTARIA"))));

            document.Add(table2);
            #endregion
            
            //Contenido tabla
            #region CONTENIDO TABLA DINAMICA
            //Separacion
            document.Add(new Paragraph(" ").SetMargins(2,0,0,0));
            float[] cellWidth3 = { 6f,4f,4f,8f,8f,8f,8f,7f,7f,8f,8f,8f,8f,8f,8f };
            Table table3 = new Table(UnitValue.CreatePercentArray(cellWidth3)).UseAllAvailableWidth();
            table3.SetFontSize(6);
            for (int i = 0; i < 10; i++)
            {
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));

                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));

                table3.AddCell(DatosCelda("MES", true, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", false, true, TextAlignment.CENTER));
                table3.AddCell(DatosCelda("MES", true, true, TextAlignment.CENTER));
            }

            document.Add(table3);
            #endregion
            
            //Totales Tabla

            #region TOTALES TABLA
            //Separacion
            document.Add(new Paragraph(" ").SetMargins(2,0,0,0));
            float[] cellWidth4 = { 6f,4f,4f,8f,8f,8f,8f,7f,7f,8f,8f,8f,8f,8f,8f };
            Table table4 = new Table(UnitValue.CreatePercentArray(cellWidth4)).UseAllAvailableWidth();
            table4.SetFontSize(6);
            
            Cell CellTotales = new Cell(1, 6).Add(LetrasCabeceraTabla("TOTALES")); 
            table4.AddCell(PropiedadesCelda(CellTotales));
            
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));
            table4.AddCell(DatosCelda("50", true, true, TextAlignment.CENTER));

            document.Add(table4);
            #endregion
            
            document.Add(LetrasBody("Cálculo de Deuda Tributaria", 8, false, false, 2, 2, TextAlignment.LEFT));
            
            
            document.Close();

            return stream.ToArray();
        }

        private Paragraph LetrasBody(string letras, int sizeLetra, bool isBold, bool isBlue, int separacionTop, int separacionBottom, TextAlignment posicionLetra)
        {
            Paragraph letrasBody = new Paragraph(letras);
            letrasBody.SetTextAlignment(posicionLetra);
            letrasBody.SetFontSize(sizeLetra);
            letrasBody.SetPaddingTop(separacionTop); //separacion con bloque de arriba
            letrasBody.SetPaddingBottom(separacionBottom);
            letrasBody.SetMarginBottom(separacionBottom);
            letrasBody.SetMarginTop(1);
            if (isBlue)
                letrasBody.SetFontColor(new DeviceRgb(0, 64, 128));
            if (isBold)
                letrasBody.SetBold();
            return letrasBody;
        }
        private Paragraph LetrasCabeceraTabla(string letras)
        {
            Paragraph text = new Paragraph(letras);
            text.SetTextAlignment(TextAlignment.CENTER);
            text.SetBold();
            return text;
        }

        private Cell PropiedadesCelda(Cell celda)
        {
            celda.SetMargin(0);
            celda.SetBackgroundColor(ColorConstants.LIGHT_GRAY);
            celda.SetPadding(0);
            celda.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            return celda;
        }

        private Cell DatosCelda(string contenidoCelda, bool isBold, bool conBorde, TextAlignment posicionLetra)
        {
            Cell datosCelda = new Cell();

            Style letraBoldnBorde = new Style().SetBorder(Border.NO_BORDER).SetBold();
            Style letrasNoBoldnBorde = new Style().SetBorder(Border.NO_BORDER);
            
            Style letraBoldBorde = new Style().SetBold().SetBackgroundColor(new DeviceRgb(229, 231, 233)).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            Style letrasNoBoldBorde = new Style().SetVerticalAlignment(VerticalAlignment.MIDDLE);

            datosCelda.Add(new Paragraph(contenidoCelda));
            datosCelda.SetTextAlignment(posicionLetra);
            datosCelda.SetPadding(2);
            if (posicionLetra == TextAlignment.RIGHT)
                datosCelda.SetPaddingRight(3);
            if (posicionLetra == TextAlignment.LEFT)
                datosCelda.SetPaddingLeft(3);
            // datosCelda.SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            //caso 1
            if (isBold && !conBorde)
                datosCelda.AddStyle(letraBoldnBorde);
            //caso 2
            if (!isBold && !conBorde)
                datosCelda.AddStyle(letrasNoBoldnBorde);
            //caso 3
            if (isBold && conBorde)
                datosCelda.AddStyle(letraBoldBorde);
            //caso 4
            if (!isBold && conBorde)
                datosCelda.AddStyle(letrasNoBoldBorde);
            // datosCelda.AddStyle(letrasNoBoldBorde).SetBorder(new SolidBorder(Color.ConvertRgbToCmyk(new DeviceRgb(0,64,128)),1));

            return datosCelda;
        }
    }
}